package uk.ac.coventry.uni.m19com.nec;

import java.util.Date;
import java.util.Locale;

/**
 * This class is used to test Operations methods
 * 
 * @author Heitor Meira - demeloh@uni.coventry.ac.uk
 * @version 05/12/2013
 */
public class EventOperationsMain {
	private final static String TEST_BLOCK_SEPARATOR = "*********************"
			+ "**************************************************************\n";
	private final static String ID = "id-to-test";
	private final static String NAME = "name-to-test";
	private final static Date DATE = EventHelper.createDate("02/02/2015");
	private final static String LAYOUT = EventHelper.genEventConfiguration();
	private final static int HALL = 5;
	private final static int NUMBER_OF_EVENTS = 25;

	/**
	 * Adds n events to the provided {@link EventList}.
	 * @param eventList The event list to have the events added.
	 * @param quantity the number of events to be added.
	 */
	private static void generateNEC_Events(EventList eventList, int quantity) {
		for (int i = 0; i < quantity; i++) {
			createNEC_Event(eventList, i);
		}
	}

	/**
	 * Adds a new event to the provided {@link EventList} with name equal
	 * name-+index, a generated layout using the {@link EventHelper} method, a
	 * generated id using the {@link EventHelper} method, a date following this
	 * pattern: index/12/2014, and fixed hall specified by the class.
	 * 
	 * @param eventList The event list to have the event added.
	 * @param index A number to generated a date and differentiate the name.
	 */
	private static void createNEC_Event(EventList eventList, int index) {
		eventList.add("name-" + index, 
				EventHelper.genEventConfiguration(), EventHelper.generateEventID(), 
				EventHelper.createDate(index + "/12/2014"), HALL);
	}

	/**
	 * Prints the current state of the provided {@link EventList}.
	 * @param eventList The event list to be printed.
	 */
	private static void printEventScheduleState(EventList eventList) {
		System.out.println("This is the the EventScheduler state:");
		System.out.println(eventList);
	}

	/**
	 * Prints a header for the test.
	 * @param label A label for the header to identify the test.
	 */
	private static void printTestHeader(String label) {
		System.out.println(String.format(Locale.getDefault(), 
				"======================== Begin of %s method tests ========================", label));
	}

	/**
	 * Prints the resume of a successful test. 
	 */
	private static void printRestulOk() {
		System.out.println("TEST RESULT: OK");
	}

	/**
	 * Prints the resume of a failed test. 
	 */
	private static void printRestulFail() {
		System.out.println("TEST RESULT: FAIL");
	}

	/**
	 * Prints the footer of a test.
	 * @param eventList The list to be shown the state.
	 * @param label A label for identification of finished test.
	 * @param isTestOk The result of the test, true if succeeded false otherwise.
	 */
	private static void printTestFooter(EventList eventList, String label, boolean isTestOk) {
		printEventScheduleState(eventList);
		System.out.println(String.format(Locale.getDefault(), 
				"======================== End of %s method tests ========================", label));
		if (isTestOk) {
			printRestulOk();
		} else {
			printRestulFail();
		}
	}

	/*
	 * add method tests
	 */
	/**
	 * Tests the size method of this implementation of {@link EventList}.
	 * @param eventList The {@link EventList} to be tested.
	 */
	private static void testSizeMethod(EventList eventList) {
		printTestHeader("size");
		boolean isTestOk = true;
		System.out.println(String.format("Initialy EventSchedule has %d events.", eventList.size()));
		if (NUMBER_OF_EVENTS == eventList.size()) {
			System.out.println("Size is OK.");
			isTestOk = true;
		} else {
			System.out.println("Size is wrong: FAIL");
			isTestOk = false;
		}

		printTestFooter(eventList, "size", isTestOk);
	}

	/*
	 * add method tests
	 */
	/**
	 * Tests the add method of this implementation of {@link EventList}. This
	 * tests a succeeded addition, a addition with past date and addition 0 >=
	 * hall && hall > 20.
	 * 
	 * @param eventList
	 *            The {@link EventList} to be tested.
	 */
	private static void testAddMethod(EventList eventList) {
		printTestHeader("add");
		int sizeBeforeAdd = eventList.size();
		System.out.println(String.format("Initialy EventSchedule has %d events.", sizeBeforeAdd));

		boolean addedWithPastDate = eventList.add(NAME, LAYOUT, ID, EventHelper.createDate("11/01/2012"), HALL);
		if (!addedWithPastDate) {
			System.out.println(String.format("After add one EventSchedule with past date returned false: OK."));
		} else {
			System.out.println(String.format("After add one EventSchedule with past date returned true: FAIL."));
		}

		boolean addedWithHallZero = eventList.add(NAME, LAYOUT, ID, DATE, 0);
		if (!addedWithHallZero) {
			System.out.println(String.format("After add one EventSchedule with hall 0 returned false: OK."));
		} else {
			System.out.println(String.format("After add one EventSchedule with hall 0 returned true: FAIL."));
		}

		boolean addedWithHall21 = eventList.add(NAME, LAYOUT, ID, DATE, 21);
		if (!addedWithHall21) {
			System.out.println(String.format("After add one EventSchedule with hall 21 returned false: OK."));
		} else {
			System.out.println(String.format("After add one EventSchedule with hall 21 returned true: FAIL."));
		}

		boolean added = eventList.add(NAME, LAYOUT, ID, DATE, HALL);
		int sizeAfterAdd = eventList.size();
		System.out.println(String.format("After add one EventSchedule has %d events and returned %s", sizeAfterAdd, added));
		if (sizeAfterAdd == sizeBeforeAdd + 1 && added) {
			System.out.println("Event added: OK.");
		} else {
			System.out.println("Event not added: FAIL.");
		}

		printTestFooter(eventList, "add", (sizeAfterAdd == sizeBeforeAdd + 1) && added && !addedWithPastDate && !addedWithHallZero);
	}

	/**
	 * Tests the add method that will fail expecting a NullPointerExeception.
	 * 
	 * @param label The label to identify this test.
	 * @param eventList The {@link EventList} to be tested.
	 * @param name The name of the event to be added.
	 * @param layout The layout of the event to be added.
	 * @param id The id of the event to be added.
	 * @param date The date of the event to be added.
	 * @param hall The hall of the event to be added.
	 */
	private static void testAddMethodNullParam(String label, EventList eventList, 
			String name, String layout, String id, Date date, int hall) {
		printTestHeader(String.format(Locale.getDefault(), "%s", label));
		boolean isTestOk = true;
		int sizeBeforeAdd = eventList.size();
		System.out.println(String.format("Initialy EventSchedule has %d events.", sizeBeforeAdd));

		try {
			eventList.add(name, layout, id, date, hall);
			System.out.println(String.format(Locale.getDefault(), "%s: FAIL", label));
			isTestOk = false;
		} catch (NullPointerException ex) {
			System.out.println(String.format(Locale.getDefault(), "NullPointerException (OK): %s", ex.getMessage()));
		}

		int sizeAfterAdd = eventList.size();
		System.out.println(String.format("After add one EventSchedule has %d events.", sizeAfterAdd));

		printTestFooter(eventList, String.format(Locale.getDefault(), "%s", label), 
				isTestOk && (sizeAfterAdd == sizeBeforeAdd));
	}

	/*
	 * get method tests
	 */
	/**
	 * Tests the get method of this implementation of {@link EventList}.
	 * @param eventList The {@link EventList} to be tested.
	 */
	private static void testGetMethod(EventList eventList) {
		boolean isTestOk = true;
		printTestHeader("get");
		
		NEC_Event event = eventList.get(ID);
		System.out.println("Event retrieved: " + event);
		if (!ID.equals(event.getID())) {
			System.out.println(String.format(Locale.getDefault(), 
					"The wrong event was retrieved his id is: %s should be: %s: FAIL", 
					ID, event.getID()));
			isTestOk = false;
		} else {
			System.out.println("IDs match");
		}
		
		printTestFooter(eventList, "get", isTestOk);
	}

	/**
	 * Tests the get method of this implementation of {@link EventList} expecting
	 * a null pointer exception. This method will try to pass a null id as
	 * argument.
	 * 
	 * @param eventList
	 *            The {@link EventList} to be tested.
	 */
	private static void testGetMethodNullId(EventList eventList) {
		printTestHeader("get id null");
		boolean isTestOk = true;

		try {
			eventList.get(null);
			System.out.println("get id null: FAIL");
			isTestOk = false;
		} catch (NullPointerException ex) {
			System.out.println(String.format(Locale.getDefault(), "NullPointerException (OK): %s", ex.getMessage()));
		}

		printTestFooter(eventList, "get id null", isTestOk );
	}

	/**
	 * Tests the get method of this implementation of {@link EventList} trying
	 * to get an event with inexistent id.
	 * 
	 * @param eventList
	 *            The {@link EventList} to be tested.
	 */
	private static void testGetInexistentIdMethod(EventList eventList) {
		boolean isTestOk = true;
		printTestHeader("get inexistent event");
		
		NEC_Event event = eventList.get("some-crazy-inexistent-id");
		System.out.println("Event retrieved: " + event);
		if (event != null) {
			System.out.println(String.format(Locale.getDefault(), 
					"Should not have found an event with inexistent id. "
					+ "It should be null but was: %s: FAIL", 
					event));
			isTestOk = false;
		} else {
			System.out.println("Event with the provided id not found. Returning null: OK");
		}
		
		printTestFooter(eventList, "get inexistent event", isTestOk);
	}

	/*
	 * remove method tests
	 */
	/**
	 * Tests the remove method of this implementation of {@link EventList} by
	 * removing an existing event and an nonexistent one.
	 * 
	 * @param eventList
	 *            The {@link EventList} to be tested.
	 */
	private static void testRemoveMethod(EventList eventList) {
		printTestHeader("remove");
		boolean isTestOk = true;
		int sizeBeforeRemove = eventList.size();
		System.out.println(String.format("Initialy EventSchedule has %d events.", sizeBeforeRemove));

		if (eventList.remove(ID)) {
			System.out.println("Event removed: OK");
		} else {
			System.out.println("Event not removed: FAIL");
			isTestOk = false;
		}
		
		if (eventList.remove(ID)) {
			System.out.println("Event was removed twice, should not remove the same event two times: FAIL");
			isTestOk = false;
		} else {
			System.out.println("Inexistent id not removed, returning false. OK.");
		}

		int sizeAfterRemove = eventList.size();
		System.out.println(String.format("After remove one EventSchedule has %d events.", sizeAfterRemove));

		printTestFooter(eventList, "remove", isTestOk && (sizeAfterRemove == sizeBeforeRemove - 1));
	}

	/**
	 * Tests the remove method of this implementation of {@link EventList}
	 * expecting a NullPointerException by providing a null id.
	 * 
	 * @param eventList
	 *            The {@link EventList} to be tested.
	 */
	private static void testRemoveMethodNullId(EventList eventList) {
		printTestHeader("remove id null");
		boolean isTestOk = true;

		try {
			eventList.remove(null);
			System.out.println("remove id null FAILED");
			isTestOk = false;
		} catch (NullPointerException ex) {
			System.out.println(String.format(Locale.getDefault(), "NullPointerException (OK): %s", ex.getMessage()));
		}

		printTestFooter(eventList, "remove id null", isTestOk);
	}

	/*
	 * sort method tests
	 */
	/**
	 * Tests the sort method of this implementation of {@link EventList}.
	 * Including the case of the list being already sorted.
	 * 
	 * @param eventList
	 *            The {@link EventList} to be tested.
	 */
	private static void testSortMethod(EventList eventList) {
		printTestHeader("sort");
		boolean isTestOk = true;
		
		EventSchedule eventSchedule = (EventSchedule) eventList;
		NEC_Event[] necEvents = EventHelper.getEventsArray(eventSchedule.cloneNEC_EventsList());
		if (EventHelper.eventsOrderCheck(necEvents)) {
			System.out.println("events should not be sorted: FAIL");
			isTestOk = false;
		} else {
			System.out.println("the events list is not sorted: OK.");
		}
		printEventScheduleState(eventList);

		if (eventSchedule.sort()) {
			System.out.println("the events has been sorted: OK");
		} else {
			System.out.println("events not sorted: FAIL");
			isTestOk = false;
		}

		if (eventSchedule.sort()) {
			System.out.println("the events should be sorted already: FAIL");
			isTestOk = false;
		} else {
			System.out.println("events already sorted returning false: OK");
		}
		necEvents = EventHelper.getEventsArray(eventSchedule.cloneNEC_EventsList());
		if (EventHelper.eventsOrderCheck(necEvents)) {
			System.out.println("events sorted: OK");
		} else {
			System.out.println("events not sorted: FAIL");
			isTestOk = false;
		}

		printTestFooter(eventList, "sort", isTestOk);
	}

	public static void main(String[] args) {
		// Print start-of-testing timestamp
		System.out.println("\n*** Begin testing on " + new Date());

		// Create new concrete class implementing EventList interface for
		// testing
		// populate it with at least 25 layout objects...
		EventList eventList = new EventSchedule();
		generateNEC_Events(eventList, NUMBER_OF_EVENTS);

		// Test implemented EventList methods add(), remove() and get()
		System.out.println("\n** Begin testing event list methods");
		// testing goes here...
		testSizeMethod(eventList);
		System.out.println(TEST_BLOCK_SEPARATOR);
		
		testAddMethod(eventList);
		System.out.println(TEST_BLOCK_SEPARATOR);
		testAddMethodNullParam("adding null name", eventList, null, LAYOUT, ID, DATE, HALL);
		System.out.println(TEST_BLOCK_SEPARATOR);
		testAddMethodNullParam("adding null layout", eventList, NAME, null, ID, DATE, HALL);
		System.out.println(TEST_BLOCK_SEPARATOR);
		testAddMethodNullParam("adding null ID", eventList, NAME, LAYOUT, null, DATE, HALL);
		System.out.println(TEST_BLOCK_SEPARATOR);
		testAddMethodNullParam("adding null date", eventList, NAME, LAYOUT, ID, null, HALL);
		System.out.println(TEST_BLOCK_SEPARATOR);

		testGetMethod(eventList);
		System.out.println(TEST_BLOCK_SEPARATOR);
		testGetMethodNullId(eventList);
		System.out.println(TEST_BLOCK_SEPARATOR);
		testGetInexistentIdMethod(eventList);
		System.out.println(TEST_BLOCK_SEPARATOR);

		testRemoveMethod(eventList);
		System.out.println(TEST_BLOCK_SEPARATOR);
		testRemoveMethodNullId(eventList);
		System.out.println(TEST_BLOCK_SEPARATOR);
		System.out.println("\n** Finish testing event list methods");

		// Test implemented EventList method sort()
		System.out.println("\n** Begin testing sort method");
		// testing goes here...
		testSortMethod(eventList);
		System.out.println(TEST_BLOCK_SEPARATOR);
		System.out.println("\n*** Finish testing sort method");

		System.out.println("\n*** Finish testing on " + new Date());

	}

}