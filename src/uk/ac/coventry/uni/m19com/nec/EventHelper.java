package uk.ac.coventry.uni.m19com.nec;

/**
 * Helper class for event operations *** DO NOT MODIFY ***
 * @author Faiyaz Doctor
 * @version 1.2 - 25/09/13
 */
 
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.List;

public class EventHelper
{   
	// used in getenation ID
    private static int iDNumber = 100;  
    
    
	// Check ordering (after sort-by-ID and converting to array), will return true if in order, false otherwise 
    public static boolean eventsOrderCheck(NEC_Event[] arr)
    {
        for(int i = 1; i < arr.length; i++)
            if(arr[i].compareTo(arr[i-1]) < 0)
            {   // not in ascending order
            	return false;
            }
        return true;
    }
    
    
    // Generate an event unique ID 
    public static String generateEventID()
    {   
        String id = genLetter() + "-" + iDNumber++ + genLetter() + genLetter() + genLetter();
        return id;
    }
    
    
    // Generate an event type
    public static String genEventConfiguration()
    {
      List<String> layouts = Arrays.asList("Conference-ConfigA", "Fair-Config1", "Concert-ConfigM", "Conference-ConfigC", "Fair-Config2", "Concert-ConfigL", "Conference-ConfigD", "Fair-Config4", "Concert-ConfigU", "Conference-ConfigB", "Fair-Config3", "Concert-ConfigV");
      String layoutConfig = layouts.get((int)(layouts.size()*Math.random()));
      return layoutConfig;
    } 
    
    
    // Create a new date object, 
    // input string date should be in the following format ("dd/MM/yyyy"), e.g. 27/09/2013
    public static Date createDate(String date)
    {
      Date d = null;
      
      try
      {
         SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
         d = sdf.parse(date);
      }
      catch(ParseException e)
      {
         System.err.println("Date is in the wrong format should be 'dd/MM/yyyy'");
         return d;
      }
      
      return d;
    }
    
    
    // check if a date is greater than current date, returns true if is greater, false otherwise
    public static boolean checkDate(Date date)
    {
      Date today = new Date();
      
      if(date.after(today))
      {
         return true;
      }
      else
      {
         return false;
      }
    }
	 
	 
    // method to convert list of NEC_Events into an array to be used by eventsOrderCheck()
	 public static NEC_Event[] getEventsArray(List<NEC_Event> list)
	 {
	   NEC_Event[] eventsArray = list.toArray(new NEC_Event[0]);
      return eventsArray;
	 }
    
    
    // generate letter utility method used in generateEventID()
    private static char genLetter()
    {
        return (char)('A' + (int)(26*Math.random()));
    }
    
}