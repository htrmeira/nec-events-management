package uk.ac.coventry.uni.m19com.nec;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * This class is a implementation of a {@link EventList} using a
 * {@link LinkedList} as main data structure, an unsorted structure as
 * specified, it is responsible for manage (add, remove, retrieve etc) the event
 * schedules by the system. Note a new method has been added to make this class
 * compatible with the {@link EventHelper} class provided.
 * 
 * @author Heitor Meira - demeloh@uni.coventry.ac.uk
 * @version 05/12/2013
 * 
 */
public class EventSchedule implements EventList {
	public final static String NEW_LINE = "\n";

	private final List<NEC_Event> events;

	/**
	 * This is the constructor of EventScheduler, this constructor creates a new
	 * and empty {@link NEC_Event} list.
	 */
	public EventSchedule() {
		events = new LinkedList<NEC_Event>();
	}

	@Override
	public boolean add(String name, String layout, String ID, Date date, int hall) throws NullPointerException {
		checkNotNull("ID can not be null", ID);
		checkNotNull("name can not be null", name);
		checkNotNull("layout can not be null", layout);
		checkNotNull("date can not be null", date);
		if (hall < 1 || hall > 20) {
			return false;
		} else if (new Date().getTime() > date.getTime()) {
			return false;
		}
		return events.add(new NEC_Event(name, layout, ID, date, hall));
	}

	@Override
	public boolean remove(String id) throws NullPointerException {
		checkNotNull("id can not be null", id);
		return events.remove(createNEC_EventStub(id));
	}

	@Override
	public NEC_Event get(String id) throws NullPointerException {
		checkNotNull("id can not be null", id);
		for (NEC_Event event : events) {
			if (id.equals(event.getID())) {
				return event;
			}
		}
		return null;
	}

	@Override
	public int size() {
		return events.size();
	}

	/**
	 * Provides a representation of the current state of this class, by
	 * returning a string with each event representation in a new line,
	 * following these pattern: S0\nS1\nS2\n... Showing the string
	 * representation of one event per line.
	 * 
	 * @return A printable string formatted of the current status of this
	 *         instance.
	 */
	@Override
	public String toString() {
		String result = new String();
		for (NEC_Event event : events) {
			result += event.toString() + NEW_LINE;
		}
		return result;
	}

	/**
	 * {@inheritDoc}. This method implements a simple insertion sort algorithm
	 * as described in <a
	 * href="http://en.wikipedia.org/wiki/Insertion_sort">
	 * http://en.wikipedia.org/wiki/Insertion_sort</a> with some modifications in
	 * order to have a boolean return indicating if the list was ordered or
	 * not.
	 */
	@Override
	public boolean sort() {
		boolean wasModified = false;
		boolean changed = true;
		while (changed) {
			changed = false;
			for (int i = 0; i < events.size() - 1; i++) {
				if (events.get(i).compareTo(events.get(i + 1)) > 0) {
					swapEvents(events, i);
					changed = true;
					wasModified = true;
				}
			}
		}
		return wasModified;
	}

	/**
	 * Returns a shallow clone {@link List} of the events that this
	 * EventSchedule contains. This method was created to make this class
	 * compatible with the {@link EventHelper} provided.
	 * 
	 * @return A shallow copy with the events that this EventSchedule contains.
	 */
	public List<NEC_Event> cloneNEC_EventsList() {
		/*
		 * POST: a shallow copy of the current system is returned.
		 */
		return new LinkedList<NEC_Event>(events);
	}

	/**
	 * Moves the element found in the index to index + 1 and the element of
	 * index + 1 to index.
	 * 
	 * @param events
	 *            The events list to swap the elements.
	 * @param index
	 *            The position of the element that needs to swap places with the
	 *            element in index + 1;
	 */
	private void swapEvents(List<NEC_Event> events, int index) {
		/*
		 * PRE: there is a list of events with 1 or more elements. 
		 * POST: the element in position n is moved to position n + 1 and the element 
		 * in position n + 1 is moved to position n.
		 */
		NEC_Event swap = events.remove(index);
		events.add(index + 1, swap);
	}

	/**
	 * Checks if an object is null, and throws an exception case it is true.
	 * 
	 * @param message
	 *            The message to be in the exception in case of the object be
	 *            null.
	 * @param obj
	 *            The object to check if is null.
	 * 
	 * @throws NullPointerException
	 *             if the object being checked is null.
	 */
	private void checkNotNull(String message, Object obj) {
		/*
		 * PRE: an object and a message is provided.
		 * POST: if the object is null, a NullPointerException is thrown 
		 * with the provided message, otherwise nothing happens.
		 */
		if (obj == null) {
			throw new NullPointerException(message);
		}
	}

	/**
	 * Creates a stub of a {@link NEC_Event} based on the way it implements
	 * equals and compareTo methods, that is based only on the id. This method
	 * created a new {@link NEC_Event} with the provided id, empty string for
	 * name, empty string for layout, the current date and 0 halls.
	 * 
	 * @param id The id of the {@link NEC_Event} to be created.
	 * @return A stub of a {@link NEC_Event} with the given id.
	 */
	private NEC_Event createNEC_EventStub(String id) {
		/*
		 * PRE: an id is provided.
		 * POST: a stub of a NEC_Event is created with empty name, layout, 
		 * current date and hall equal 1. The id of the event is the on provided.
		 */
		return new NEC_Event(new String(), new String(), id, new Date(), 1);
	}

}
