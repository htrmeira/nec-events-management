package uk.ac.coventry.uni.m19com.nec;
/**
 * Interface for a class representing a list of events *** DO NOT MODIFY ***
 * @author Faiyaz Doctor
 * @version 1.2 - 02/10/13
 */
 
import java.util.Date;

public interface EventList
{
    /**
     * Adds an event to the List, if its not already there
     * @param name string  the name of the event
     * @param layout string  layout configuration of the event
     * @param id string  unique ID for each event 
     * @param date Date date object (must be > the current date)
     * @param hall int  hall number (must be <0 <= 20)
     * @return  true if successful, else false
     * @throws NullPointerException  if name, type, ID, date are null
     */
    public boolean add(String name, String layout, String ID, Date date, int hall) throws NullPointerException;
    // POST: if name, layout, ID or date is null the exception is thrown; else
    // the a new event is created and added to our list and true is returned,
    // or false if the event was not added for some reason
    
    
    /**
     * Remove an event from the list
     * @param id  string  the id of the event to remove
     * @return true if successful, else false
     * @throws NullPointerException if id is null
     */
    public boolean remove(String id) throws NullPointerException;
    // POST: if string is null the exception is thrown; othewise
    // if the event is not in our list false is returned, else
    // matching instance of the event is removed and true is returned
    
    
    /**
     * Get an event given the event's id
     * @param id string  the event's id string
     * @return  the event if found, else null
     */
    public NEC_Event get(String id) throws NullPointerException;
    // POST: if string is null the exception is thrown; otherwise
    // if an layout is found matching the specified id 
    // then that layout is returned, else null
    
    
    /**
     * Get the size of the list (number of events)
     * @return  the list size
     */
    public int size();
    // POST: the list size (= number of events) is returned
    
    
    /**
     * Convert the list to a single string "[a0, a1, a2...]"
     * @return a printable string formatted as above
     */
    public String toString();
    // POST: a string is returned of the form S0\nS1\nS2\n...
    // where each Si is the string representation of ith event
    
    
    /**
     * Sort the list of event into ascending order by ID
     * @return true if successful, else false
     */
    public boolean sort();
    // POST: the list of events are sorted in ascending order by registration
    // if the list is not sorted false is returned, else true is returned

}