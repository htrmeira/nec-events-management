package uk.ac.coventry.uni.m19com.nec;
/**
 * Class to represent individual Events *** DO NOT MODIFY ***
 * @author Faiyaz Doctor
 * @version 1.2 - 25/09/13
 */
 
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class NEC_Event implements Comparable<NEC_Event>
{
    private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    private String name;      // name of the event
    private String layout;    // layout configuration of the event
    private Date date;        // Date of the event
    private String ID;        // unique event ID
    private int hall;         // number of hall
    
    
    /**
     * Constructor
     */
    public NEC_Event(String name, String layout, String id, Date date, int hall)
    {
    	  this.name = name;
        this.layout = layout; 
        this.ID = id;
        this.date = date;
        this.hall = hall;
    }
    
    
    /**
     * Get the event's name
     * @return the name string
     */
    public String getName()
    {
        return name;
    }
    
    
    /**
     * Get the event' layout
     * @return the layout string
     */
    public String getLayout()
    {
        return layout;
    }
    
    
    /**
     * Sets the event' layout configuration
     * @return a string notification 
     */
    public String setLayout()
    {
        return new String("Configuring layout according to event selected layout....");
    }

    
    
    /**
     * Get the event's ID
     * @return the event's ID string
     */
    public String getID()
    {
        return ID;
    }
    
        
     /**
     * Get the event's date
     * @return the event's date string
     */
    public Date getDate()
    {
        return date;
    }
    
    
    /**
     * Get the event's hall
     * @return the hall string
     */
    public int getHall()
    {
        return hall;
    }

    
    /**
     * Implement Comparable based on ID comparisons
     * @param other the other event
     * @return the result of the ID comparision
     */
    public int compareTo(NEC_Event other)
    {
        return this.getID().compareTo(other.getID());
    }
    
    
    /**
     * Override the default equals(), based on IDs
     * @param obj  the other Object
     * @return true if obj has same ID, else false
     */
    public boolean equals(Object obj)
    {
        if(!(obj instanceof NEC_Event)) return false;
        return this.ID.equals(((NEC_Event)obj).ID);
    }
    
    
    /**
     * Create a string with the layout ID, name, layout, start date, and hall number
     * concatenated in that order, separated by colons ':'
     * @return  a printable string as above 
     */
    public String toString()
    {
        return ID + ":" + name + ":" + layout + ":" + df.format(date) + ":" + hall;
    }
}