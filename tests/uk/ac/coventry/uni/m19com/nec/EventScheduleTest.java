package uk.ac.coventry.uni.m19com.nec;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EventScheduleTest {
	private EventSchedule schedule;

	@Before
	public void setUp() {
		schedule = new EventSchedule();
	}

	@Test
	public void sortMethodTest() {
		assertFalse("An empty list is already sorted.", schedule.sort());
		createStubNEC_Event(schedule, "3", 0);
		assertFalse("A list with one object is already sorted.", schedule.sort());
		createStubNEC_Event(schedule, "4", 0);
		createStubNEC_Event(schedule, "1", 0);
		createStubNEC_Event(schedule, "2", 0);

		List<String> eventIds = toStringToEventIds(schedule.toString());
		assertEquals("id-3", eventIds.get(0));
		assertEquals("id-4", eventIds.get(1));
		assertEquals("id-1", eventIds.get(2));
		assertEquals("id-2", eventIds.get(3));

		assertTrue("Should not be sorted yet.", schedule.sort());

		eventIds = toStringToEventIds(schedule.toString());
		assertEquals("id-1", eventIds.get(0));
		assertEquals("id-2", eventIds.get(1));
		assertEquals("id-3", eventIds.get(2));
		assertEquals("id-4", eventIds.get(3));

		assertFalse("Should be sorted already.", schedule.sort());
		assertEquals("id-1", eventIds.get(0));
		assertEquals("id-2", eventIds.get(1));
		assertEquals("id-3", eventIds.get(2));
		assertEquals("id-4", eventIds.get(3));
	}

	private void createStubNEC_Event(EventSchedule schedule, String id, int hall) {
		schedule.add("name-" + id, "layout-" + id, "id-" + id, new Date(), hall);
	}

	private List<String> toStringToEventIds(String eventScheduleToString) {
		List<String> eventsIds = new LinkedList<String>();
		for (String event : eventScheduleToString.split(EventSchedule.NEW_LINE)) {
			String[] eventFields = event.split(":");
			eventsIds.add(eventFields[0]);
		}
		return eventsIds;
	}

}
